const cron = require("node-cron");
const cleanUpSchedules = require("./services/cleanUpScheduleServices");

//Agendamento da limpeza 
//* regra de agendamento 
cron.schedule(" 0 0 * * *",async()=>{
   try{
       await cleanUpSchedules();
       console.log("Limpeza automática executada")
   }catch (error){
    console.error("Erro ao executar limpeza automática",error)
   }
});