const connect = require("../db/connect");

async function cleanUpSchedules(){
    const currenDate = new Date(); 
    
    //Definir a data para 7 dias atrás
    currenDate.setDate(currenDate.getDate()- 7);

    //Formata a data para YYYY-MM-DD (padrão americano: year,month,day)
    const formattedDate = currenDate.toISOString().split('T')[0];

    const query =  `DELETE FROM schedule WHERE dateEnd < ?`
    const values = [formattedDate];

    return new Promise((resolve, reject)=>{
        connect.query(query,values, function(err, results){
            if(err){
                console.error("Erro Mysql", err);
                return reject (new Error ("Erro ao limpar agendamento"));
            }
            console.log("Agendamentos antigos apagados");
            resolve("Agendamentos antigos limpos com sucesso"); 
        });
    });
}
module.exports = cleanUpSchedules;