const connect = require("../db/connect");
module.exports = class classroomController {
  static async createClassroom(req, res) {
    const { number , description, capacity } = req.body;

    // Verifica se todos os campos estão preenchidos
    if (!number || !description || !capacity) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }

    // Caso todos os campos estejam preenchidos, realiza a inserção na tabela
    const query = `INSERT INTO classroom (number, description, capacity) VALUES ( 
        '${number}', 
        '${description}', 
        '${capacity}'
      )`;

    try {
      connect.query(query, function (err) {
        if (err) {
          console.log(err);
          res.status(500).json({ error: "Erro ao cadastrar sala" });
          return;
        }
        console.log("Sala cadastrada com sucesso");
        res.status(201).json({ message: "Sala cadastrada com sucesso" });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async getAllClassroom(req, res) {
    try {
        const query = "SELECT * FROM classroom";
        connect.query(query, function (err, result) {
            if (err) {
                console.error("Erro ao obter salas:", err);
                return res.status(500).json({ error: "Erro interno do servidor" });
            }
            console.log("Salas obtidas com sucesso");
            res.status(200).json({ message: "Obtendo todas as salas", classrooms: result });
        });
    } catch (error) {
        console.error("Erro ao executar a consulta:", error);
        res.status(500).json({ error: "Erro interno do servidor" });
    }
}


static async getClassroomById(req, res) {
  const classroomId = req.params.number;

  try {
      const query = `SELECT * FROM classroom WHERE number = '${classroomId}'`;
      connect.query(query, function (err, result) {
          if (err) {
              console.error("Erro ao obter sala:", err);
              return res.status(500).json({ error: "Erro interno do servidor" });
          }

          if (result.length === 0) {
              return res.status(404).json({ error: "Sala não encontrada" });
          }

          console.log("Sala obtida com sucesso");
          res.status(200).json({ message: "Obtendo a sala com ID: " + classroomId, classroom: result[0] });
      });
  } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
  }
}


  static async updateClassroom(req, res) {
    const classroomId = req.params.number;
    const { number , description, capacity } = req.body;
    // EM UMA API REAL É NECESSÁRIO FAZER ALGO NESTA LINHA
    res.status(200).json({
      message: "Sala atualizada com ID: " + classroomId,
      user: { number , description, capacity },
    });
  }

  static async deleteClassroom(req, res) {
    const classroomId = req.params.number;
    res.status(200).json({ message: "Sala excluida com ID: " + classroomId });
  }
};
